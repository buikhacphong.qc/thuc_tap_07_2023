import 'package:animated_splash_screen/animated_splash_screen.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:product_layout_app/bottomnavigation.dart';


//design layout
void main() {
  SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle(
    statusBarColor: Color(0xFF6f9bd4),
  ));
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      
      debugShowCheckedModeBanner: false,
      //title: 'Layout demo',
      theme: ThemeData(
        //fontFamily: GoogleFonts.nunitoSans().fontFamily,
        appBarTheme: AppBarTheme(
          systemOverlayStyle: SystemUiOverlayStyle(
            statusBarColor: Color(0xFF6f9bd4),
          )
        ),
        scaffoldBackgroundColor: Colors.white,
        //primarySwatch: Colors.black,
      ),
      //home: welcomeScreen(),
      home: splashScreen(),
    );
  }
}

class splashScreen extends StatelessWidget{
  const splashScreen({super.key});

  @override
  Widget build(BuildContext context){
    return AnimatedSplashScreen(
      splash: Image.asset( 
            'assets/logo4.png',
          ), 
      nextScreen: const BottomNavigationBarHome(),
      splashIconSize: 150,
      );
  }
}
// splash: Column(
//         mainAxisAlignment: MainAxisAlignment.center,
//         children: [
//           Image.asset( 
//             'assets/logo4.png',
//             //fit: BoxFit.fill,
//             width: MediaQuery.of(context).size.width/3,
//             height: MediaQuery.of(context).size.height/3,
//           ),
//           const Text(
            
//             'QUẢN LÝ \n LỊCH HẸN KHÁM',
//             style: TextStyle(
//               fontSize: 20,
//               fontWeight: FontWeight.bold,
//               color: Color(0xFF6f9bd4),
//             ),
//           ),
//         ],
//       ),


