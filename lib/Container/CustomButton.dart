import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

class CustomButton extends StatelessWidget {
  final double width;
  final double height;
  final String textButton;
  final SvgPicture buttonIcon;
  final VoidCallback onPressed;
  CustomButton(
      {super.key,
      required this.width,
      required this.height,
      required this.textButton,
      required this.buttonIcon,
      required this.onPressed});

  @override
  Widget build(BuildContext context) {
    return Container(
        width: width,
        height: height,
        child: ElevatedButton(
          // ignore: sort_child_properties_last
          child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              ColorFiltered(
                colorFilter: ColorFilter.mode( textButton == 'Từ chối' ? Color(0xFF6F9BD4):Colors.white,BlendMode.srcIn),
                child: buttonIcon,
              ),
              SizedBox(width: 5,),
              Text(textButton)
            ],
          ),
          onPressed: onPressed,
          style: ButtonStyle(
              backgroundColor: MaterialStatePropertyAll(textButton == 'Từ chối' ? Colors.white : Color(0xFF6F9BD4)),
              foregroundColor: MaterialStatePropertyAll(textButton == 'Từ chối' ? Color(0xFF6F9BD4):Colors.white  ),
              side: MaterialStatePropertyAll(BorderSide(color: Color(0xFF6F9BD4))),
              shape: MaterialStatePropertyAll(RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(8)))),
        ));
  }
}
