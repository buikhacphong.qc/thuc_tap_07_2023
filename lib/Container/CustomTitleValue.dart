import 'package:flutter/cupertino.dart';

class CustomTitleValue extends StatelessWidget {
  final String key1;
  final String value1;

  const CustomTitleValue({
    super.key,
    required this.key1,
    required this.value1,
  });

  @override
  Widget build(BuildContext context) {
    Color colorValue;
    Color colorTitle;
    if (value1 == 'Đến khám muộn') {
      // textColor = Color(0xFF5CBBB8);
      colorValue = Color(0xFFB43939);
    } else if (value1 == 'Đã đến khám') {
      colorValue = Color(0xFF5CBBB8);
    } else {
      colorValue = Color(0xFF535858);
    }

    if (key1 == 'Số thẻ BHYT (*)') {
      colorTitle = Color(0xFFB43939);
      colorValue = Color(0xFFB43939);
    } else {
      colorTitle = Color(0xFF535858);
    }
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        Container(
          width: 300 / 3 + 30,
          height: 20,
          // width: MediaQuery.of(context).size.width/2,
          // height: MediaQuery.of(context).size.height/2,
          child: Text(key1, style: TextStyle(fontSize: 11, color: colorTitle)),
        ),
        Container(
          width: 300 / 3 * 2 - 30,
          height: 20,
          // width: MediaQuery.of(context).size.width/2,
          // height: MediaQuery.of(context).size.height/2,
          child:
              Text(value1, style: TextStyle(fontSize: 11, color: colorValue)),
        )
      ],
    );
  }
}
