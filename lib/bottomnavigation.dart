import 'package:flutter/material.dart';
import 'package:product_layout_app/homescreen.dart';

//import 'package:product_layout_app/mainScreen.dart';


class BottomNavigationBarHome extends StatefulWidget {
  const BottomNavigationBarHome({super.key});

  @override
  State<BottomNavigationBarHome> createState() =>
      _BottomNavigationBarHomeState();
}

class _BottomNavigationBarHomeState extends State<BottomNavigationBarHome> {
  int _currentIndex = 0;

  final tabs = [
    mainScreen(),
    Center(child: Text('Cài đặt')),
    //accountant(),
    Center(child: Text('Tài khoản')),
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: tabs[_currentIndex],
      bottomNavigationBar: BottomNavigationBar(
        currentIndex: _currentIndex,
        type: BottomNavigationBarType.fixed,
        iconSize: 30,
        selectedIconTheme: IconThemeData(color: Color(0xFF6f9bd4),),
        fixedColor: Colors.black,
        items: [
          BottomNavigationBarItem(
            label: 'Trang chủ', //don't use title, use label
            icon: Icon(Icons.home),
            
            
          ),
          BottomNavigationBarItem(
            label: 'Cài đặt',
            icon: Icon(Icons.settings),
          ),
          
          BottomNavigationBarItem(
            label: 'Tài khoản',
            icon: Icon(Icons.person),
          ),
        ],
        onTap: (index) {
          setState(() {
            _currentIndex = index;
          });
        },
      ),
    );
  }
}
