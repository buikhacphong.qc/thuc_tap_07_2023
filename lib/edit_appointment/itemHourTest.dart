import 'package:flutter/material.dart';

class ItemGridViewTest extends StatefulWidget {
  ItemGridViewTest(
      {required this.onItemClick, required this.colorPick, super.key});
  final Function(String) onItemClick;
  Map<String, bool> colorPick;
  // final ValueChanged<String> onDataReceived;
  @override
  State<ItemGridViewTest> createState() => _ItemGridViewTestState();
}

class _ItemGridViewTestState extends State<ItemGridViewTest> {
  String itemPickedHour = '';
  static List<String> chotrong = [
    '08:00 AM',
    '08:30 AM',
    '09:00 AM',
    '09:30 AM',
    '10:00 AM',
    '10:30 AM',
    '14:00 AM',
    '14:30 AM',
    '15:00 AM',
    '15:30 AM',
    '16:00 AM',
  ];
  Color _colorBackGround = Colors.white;
  Color _colorText = Colors.blue;
  @override
  Widget build(BuildContext context) {
    return Container(
      height: 120,
      margin: const EdgeInsets.all(5),
      child: GridView.builder(
        gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(
          crossAxisCount: 3,
          childAspectRatio: 2 / 1,
        ),
        itemCount: chotrong.length,
        itemBuilder: (BuildContext context, int index) {
          return Container(
            decoration: const BoxDecoration(
              borderRadius: BorderRadius.all(Radius.circular(10)),
            ),
            margin: const EdgeInsets.all(5),
            child: GestureDetector(
              onTap: () {
                itemPickedHour = chotrong[index];
                widget.onItemClick(itemPickedHour.toString());
                setState(() {
                  for (int i = 0; i < chotrong.length; i++) {
                    if (i == index) {
                      widget.colorPick[chotrong[i]] = true;
                    } else {
                      widget.colorPick[chotrong[i]] = false;
                    }
                  }
                });
                // onDataReceived(itemPickedHour);
                // print(itemPickedHour);
              },
              child: Card(
                color: widget.colorPick[chotrong[index]] == true
                    ? Colors.blue
                    : Colors.white,
                shape: RoundedRectangleBorder(
                  side: const BorderSide(color: Colors.blue, width: 1),
                  borderRadius: BorderRadius.circular(5),
                ),
                child: Center(
                  child: Text(
                    chotrong[index],
                    style: TextStyle(
                      color: widget.colorPick[chotrong[index]] == true
                          ? Colors.white
                          : Colors.blue,
                    ),
                  ),
                ),
              ),
            ),
          );
        },
      ),
    );
  }
}
