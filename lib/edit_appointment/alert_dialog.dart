import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

class AlertPopup extends StatefulWidget {
  const AlertPopup({super.key});

  @override
  State<AlertPopup> createState() => _AlertPopupState();
}

class _AlertPopupState extends State<AlertPopup> {
  @override
  Widget build(BuildContext context) {
    return Align(
      alignment: Alignment.center,
      child: Container(
        width: 300,
        height: 220,
        decoration: BoxDecoration(
          borderRadius: BorderRadius.all(Radius.circular(8)),
          color: Colors.white,
        ),
        child: Column(children: <Widget>[
          Padding(
            padding: EdgeInsets.only(top: 10, bottom: 0, right: 5, left: 270),
            child: GestureDetector(
                onTap: () {
                  Navigator.pop(context);
                },
                child: SvgPicture.asset('assets/cancel.svg')),
          ),
          Padding(
              padding: EdgeInsets.symmetric(vertical: 7),
              child: SvgPicture.asset('assets/sad.svg')),
          Padding(
            padding: EdgeInsets.symmetric(vertical: 5),
            child: Text(
              'BẠN CHƯA CHỌN GIỜ HẸN',
              style: TextStyle(
                color: Color(0xFF444444),
                fontSize: 20,
              ),
            ),
          ),
          Padding(
              padding: EdgeInsets.only(top: 10, bottom: 0, left: 15, right: 15),
              child: Column(children: [
                ElevatedButton(
                  // ignore: sort_child_properties_last
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [Text('OK')],
                  ),
                  onPressed: () {
                    Navigator.of(context).pop();
                  },
                  style: ButtonStyle(
                      backgroundColor:
                          MaterialStatePropertyAll(Color(0xFF6F9BD4)),
                      foregroundColor: MaterialStatePropertyAll(Colors.white),
                      side: MaterialStatePropertyAll(
                          BorderSide(color: Color(0xFF6F9BD4))),
                      shape: MaterialStatePropertyAll(RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(8)))),
                ),
              ])),
        ]),
      ),
    );
  }
}
